Pour l'accès à un emploi de l'Etat, des collectivités territoriales, des établissements publics et des entreprises publiques dont le personnel est soumis à un statut réglementaire, la limite d'âge est reculée d'un temps égal au temps effectif du volontariat international.

Ce temps effectif de volontariat est pris en compte dans le calcul :

1° De l'ancienneté de service exigée pour l'accès aux concours mentionnés au 2° de l'article 19 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, au 2° de l'article 36 de la loi n° 84-53 du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale et au 2° de l'article 29 de la loi n° 86-33 du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière ;

2° De l'ancienneté exigée pour l'avancement.
