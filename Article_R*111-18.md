Dans les circonscriptions consulaires où la dispersion des résidents français le justifie, le recensement est effectué, chaque année, en une seule fois. Ces circonscriptions sont déterminées par arrêté du ministre des affaires étrangères et du ministre de la défense.

La période de recensement est alors fixée par l'ambassadeur compétent.
