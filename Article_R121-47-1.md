Les organismes agréés en application de l'article L. 120-30 perçoivent une aide pour l'organisation de la formation civique et citoyenne prévue à l'article L. 120-14.

Le montant de l'aide pour chaque personne volontaire ayant souscrit un engagement de service civique est fixé par arrêté du ministre chargé de la jeunesse et du ministre chargé du budget.

L'aide fait l'objet d'un versement unique, au terme du deuxième mois de réalisation effective de la mission.

L'aide est subordonnée à la délivrance effective de la formation civique et citoyenne à la personne volontaire.
