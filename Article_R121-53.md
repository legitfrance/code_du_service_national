Les articles R. 121-27 à R. 121-32 du présent code sont applicables à Mayotte sous réserve des adaptations suivantes :

1° Au 2e alinéa de l'article R. 121-27 les mots : “ L. 3262-1 du code du travail ” sont remplacés par les mots : “ L. 147-1 du code du travail applicable à Mayotte ” ;

2° A l'avant-dernier alinéa de l'article R. 121-28 les mots : “ R. 3262-4 du code du travail ” sont remplacés par les mots : “ R. 147-4 du code du travail applicable à Mayotte ” ;

3° Au dernier alinéa de l'article R. 121-29 les mots : “ R. 3262-13 du code du travail ” sont remplacés par les mots : “ R. 147-13 du code du travail applicable à Mayotte ” ;

4° Au deuxième alinéa de l'article R. 121-32 les mots : “ R. 3262-26 du code du travail ” sont remplacés par les mots : “ R. 147-26 du code du travail applicable à Mayotte ” ;

5° Au dernier alinéa de l'article R. 121-32 les mots : “ R. 3262-27 du code du travail ” sont remplacés par les mots : “ R. 147-27 du code du travail applicable à Mayotte ”.
