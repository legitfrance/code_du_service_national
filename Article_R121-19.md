Le congé annuel peut être pris soit par fraction, à concurrence des droits acquis, soit en une fois, en fin d'engagement ou de volontariat.
