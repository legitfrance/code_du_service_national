Le ministre de l'intérieur fixe chaque année le nombre et la durée des engagements spéciaux qui peuvent être souscrits en application des dispositions de l'article L. 94-14.
