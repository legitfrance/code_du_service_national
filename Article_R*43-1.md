Les personnes convoquées bénéficient, pendant les opérations de sélection ou lors de leur hospitalisation, de l'alimentation et du logement.
