Les Français qui ont accompli un volontariat dans les armées restent disponibles dans la réserve militaire, dans la limite de cinq ans à compter de la fin de leur volontariat.
