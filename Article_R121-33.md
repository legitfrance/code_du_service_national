L'agrément d'engagement de service civique est accordé pour une durée maximale de trois ans renouvelable aux organismes mentionnés au premier alinéa du II de l'article L. 120-1 qui :

1° Justifient d'au moins une année d'existence, sauf dérogation accordée par l'Agence du service civique au regard de l'intérêt des missions présentées par l'organisme d'accueil ;

2° Précisent le nombre de volontaires qu'ils entendent accueillir et les modalités de leur accompagnement ;

3° Précisent, le cas échéant, les modalités d'accompagnement spécifiques des volontaires mineurs de plus de seize ans ;

4° Proposent des missions d'intérêt général reconnues prioritaires pour la nation et justifient de leur capacité à les exercer dans de bonnes conditions ;

5° Disposent, y compris lorsque les missions se déroulent à l'étranger, d'une organisation et des moyens compatibles avec la formation, l'accompagnement et la prise en charge des volontaires qu'ils envisagent d'accueillir ou de mettre à disposition ;

6° Présentent un budget en équilibre et une situation financière saine dans la limite des trois derniers exercices clos, sauf dérogation accordée sur la durée d'existence par l'Agence du service civique.
