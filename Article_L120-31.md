Les organismes sans but lucratif de droit français agréés auprès desquels des personnes volontaires ont souscrit un engagement de service civique peuvent percevoir une aide, à la charge de l'Agence du service civique, aux fins de couvrir une partie des coûts relatifs à l'accueil et à l'accompagnement du volontaire accomplissant son service.



Le montant et les modalités de versement de l'aide de l'Agence du service civique, dont le niveau peut varier en fonction des conditions d'accueil de la personne volontaire et selon que l'engagement de service civique est effectué en France métropolitaine, dans un département d'outre-mer, une collectivité d'outre-mer, en Nouvelle-Calédonie, en Polynésie française, dans les îles Wallis et Futuna et les Terres australes et antarctiques françaises ou à l'étranger, sont définis par décret.
